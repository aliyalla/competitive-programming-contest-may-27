### Problem Statement 0

Sort numbers in non-decreasing order. First line of input **t** is the number of test cases. Each of the following **t** pairs of lines has N in the first and N space separated integers in the second.

**Sample stdin**

```
2
5
6 9 -10 -7 -3
10
-3 -2 6 0 6 7 8 8 5 -7
```

**Sample stdout**

```
-10 -7 -3 6 9
-7 -3 -2 0 5 6 6 7 8 8
```
